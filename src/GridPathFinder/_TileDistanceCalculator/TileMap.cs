﻿using System;
using System.Collections.Generic;

namespace GridPathFinder
{
    internal sealed class TileMap
    {
        /// <summary>
        ///     Indices: [row][col]
        /// </summary>
        private readonly Dictionary<int, Dictionary<int, Tile>> tileMap = new Dictionary<int, Dictionary<int, Tile>>();

        internal TileMap(IEnumerable<Tile> tiles)
        {
            if (tiles == null)
                throw new ArgumentNullException(nameof(tiles));

            foreach (Tile tile in tiles)
                this[tile.Row, tile.Column] = tile;
        }

        internal Tile this[int row, int column]
        {
            get => GetAt(row, column);
            private set => SetAt(row, column, value);
        }

        internal Tile GetRightNeighborOf(Tile tile) => this[tile.Row, tile.Column + 1];
        internal Tile GetLeftNeighborOf(Tile tile) => this[tile.Row, tile.Column - 1];
        internal Tile GetUpNeighborOf(Tile tile) => this[tile.Row + 1, tile.Column];
        internal Tile GetDownNeighborOf(Tile tile) => this[tile.Row - 1, tile.Column];

        private Tile GetAt(int row, int column)
        {
            if (!tileMap.TryGetValue(row, out Dictionary<int, Tile> tilesInRow))
                return null;

            if (!tilesInRow.TryGetValue(column, out Tile tile))
                return null;

            return tile;
        }

        private void SetAt(int row, int column, Tile value)
        {
            if (!tileMap.TryGetValue(row, out Dictionary<int, Tile> tilesInRow))
            {
                tilesInRow = new Dictionary<int, Tile>();
                tileMap[row] = tilesInRow;
            }

            tilesInRow[column] = value;
        }
    }
}