﻿using GridPathFinder.Utilities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;

namespace GridPathFinder
{
    public sealed class TileDistanceCalculator
    {
        /// <remarks>
        /// Contains only (all) empty and target tile(s) after initialization.
        /// While processing, processed tiles are removed from it.
        /// </remarks>>
        private readonly LinkedList<Tile> tilesToProcess;

        private readonly TileMap tileMap;
        private readonly Tile selfTile;
        private readonly Dictionary<Tile, int> tileDictionary = new Dictionary<Tile, int>();
        private bool isCalculateTileDistancesCalled = false;

        public TileDistanceCalculator(IEnumerable<Tile> tiles)
        {
            TileCollectionValidator.Validate(tiles);
            tilesToProcess = new LinkedList<Tile>(tiles);
            tileMap = new TileMap(tilesToProcess);

            // IDEA: does it increase performance if the multiple enumeration of tilesToProcess
            // in the following lines are done in one enumeration? (see First, remove, and Initialize call)
            selfTile = tilesToProcess.First(t => t.Type == TileType.Self);
            tileDictionary[selfTile] = 0;
            tilesToProcess.Remove(selfTile);
            InitializeTileDictionary();
        }

        public Dictionary<Tile, int> CalculateTileDistances()
        {
            if (isCalculateTileDistancesCalled)
                throw new InvalidOperationException($"Method '{nameof(CalculateTileDistances)} can only be called once.");

            isCalculateTileDistancesCalled = true;

            SetValuesToOneAroundSelf();
            ExpandUntilFinished();
            FillUpEmptyChambers();

            return tileDictionary;
        }

        /// <summary>
        ///     Sets initial tile values in the dictionary:                         <br/>
        ///         Empty:  0                                                       <br/>
        ///         Wall:  -1                                                       <br/>
        ///     Also removes wall tiles from <see cref="tilesToProcess"/>.
        /// </summary>
        private void InitializeTileDictionary()
        {
            LinkedListNode<Tile> current = tilesToProcess.First;
            while (current != null)
            {
                if (current.Value.Type == TileType.Wall)
                {
                    tileDictionary[current.Value] = -1;

                    // Remove "current" from the linked list ↓
                    var toRemove = current;
                    current = current.Next;
                    tilesToProcess.Remove(toRemove);
                }
                else
                {
                    tileDictionary[current.Value] = 0;
                    current = current.Next;
                }
            }
        }

        /// <summary>
        ///     Takes the self tile's all neighbors and sets their value to 1.
        /// </summary>
        private void SetValuesToOneAroundSelf()
        {
            Tile right = tileMap.GetRightNeighborOf(selfTile);
            Tile left = tileMap.GetLeftNeighborOf(selfTile);
            Tile up = tileMap.GetUpNeighborOf(selfTile);
            Tile down = tileMap.GetDownNeighborOf(selfTile);

            SetTileTo1_IfNotWall(right);
            SetTileTo1_IfNotWall(left);
            SetTileTo1_IfNotWall(up);
            SetTileTo1_IfNotWall(down);

            // ---------------------------------------------
            void SetTileTo1_IfNotWall(Tile tile)
            {
                if (tile != null && tile.Type != TileType.Wall)
                {
                    tileDictionary[tile] = 1;
                    tilesToProcess.Remove(tile);
                }
            }
        }

        private void ExpandUntilFinished()
        {
            bool didExpandPreviously = true;
            while (didExpandPreviously)
            {
                didExpandPreviously = ExpandOnce();
            }
        }

        /// <returns>Whether it expanded (true) or stayed the same (false)</returns>
        private bool ExpandOnce()
        {
            LinkedList<(Tile, int)> newValues = new LinkedList<(Tile, int)>();

            bool didExpand = false;

            // IDEA: let's remove the following local to reduce allocation?
            LinkedList<Tile> tilesToRemove = new LinkedList<Tile>();

            foreach (Tile tile in tilesToProcess)
            {
                int min = int.MaxValue;

                int leftValue = ValueOf(tileMap.GetLeftNeighborOf(tile));
                if (leftValue > 0 && leftValue < min)
                    min = leftValue;

                int rightValue = ValueOf(tileMap.GetRightNeighborOf(tile));
                if (rightValue > 0 && rightValue < min)
                    min = rightValue;

                int upValue = ValueOf(tileMap.GetUpNeighborOf(tile));
                if (upValue > 0 && upValue < min)
                    min = upValue;

                int downValue = ValueOf(tileMap.GetDownNeighborOf(tile));
                if (downValue > 0 && downValue < min)
                    min = downValue;

                if (min != int.MaxValue)
                {
                    newValues.AddFirst((tile, min + 1));
                    tilesToRemove.AddFirst(tile);
                    didExpand = true;
                }
            }

            foreach (Tile tile in tilesToRemove)
                tilesToProcess.Remove(tile);

            foreach((Tile tile, int value) in newValues)
                tileDictionary[tile] = value;

            return didExpand;

            // -----------
            int ValueOf(Tile t) => t == null ? -1 : tileDictionary[t];
        }

        private void FillUpEmptyChambers()
        {
            // There may be unreachable "chambers" in the map for which values should be set to unreachable (-1)
            tilesToProcess.ForEach(t => tileDictionary[t] = -1);
        }

        [ExcludeFromCodeCoverage]
        private void PrintTileDictionaryToOutput()
        {
            Debug.WriteLine("");
            int maxRow = tilesToProcess.Max(x => x.Row);
            int maxCol = tilesToProcess.Max(x => x.Column);

            int[,] map = new int[maxRow + 1, maxCol + 1];
            tilesToProcess.ForEach(t => map[t.Row, t.Column] = tileDictionary[t]);

            for (int row = 0; row < map.GetLength(0); row++)
            {
                StringBuilder builder = new StringBuilder();
                for (int col = 0; col < map.GetLength(1); col++)
                {
                    if (map[row, col] == -1)
                        builder.Append('#');
                    else
                        builder.Append(map[row, col]);
                }

                Debug.WriteLine(builder.ToString());
            }

            Debug.WriteLine("");
        }
    }
}