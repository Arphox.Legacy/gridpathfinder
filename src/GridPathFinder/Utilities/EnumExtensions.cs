﻿using System;

namespace GridPathFinder.Utilities
{
    public static class EnumExtensions
    {
        public static bool IsInRange<T>(this T value) where T : struct, Enum
        {
            // warning: value is boxed here  ↓↓↓↓↓
            return Enum.IsDefined(typeof(T), value);
        }
    }
}