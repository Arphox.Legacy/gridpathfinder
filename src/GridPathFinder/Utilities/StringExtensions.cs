﻿using System;

namespace GridPathFinder.Utilities
{
    public static class StringExtensions
    {
        /// <summary>
        ///     Converts the string to a 2 dimensional char array (<see cref="char[,]"/>),
        ///     finding new lines based on the <see cref="Environment.NewLine"/> value.
        /// </summary>
        /// <param name="input">The input string to convert.</param>
        /// <returns>
        ///     A <see cref="char[,]"/> where dimension 0 indicates rows, dimension 1 indicates columns.
        /// </returns>
        public static char[,] To2DCharArray(
            this string input,
            StringSplitOptions stringSplitOptions = StringSplitOptions.RemoveEmptyEntries)
        {
            return To2DCharArray(input, new string[] { Environment.NewLine }, stringSplitOptions);
        }

        /// <summary>
        ///     Converts the string to a 2 dimensional char array (<see cref="char[,]"/>),
        ///     finding new lines based on the <paramref name="separators"/> given.
        /// </summary>
        /// <param name="input">The input string to convert.</param>
        /// <param name="separators">Array of separator strings</param>
        /// <returns>
        ///     A <see cref="char[,]"/> where dimension 0 indicates rows, dimension 1 indicates columns.
        /// </returns>
        public static char[,] To2DCharArray(
            this string input,
            string[] separators,
            StringSplitOptions stringSplitOptions = StringSplitOptions.RemoveEmptyEntries)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));
            if (input == string.Empty)
                throw new ArgumentException($"{nameof(input)} cannot be empty.");
            if (separators == null)
                throw new ArgumentNullException(nameof(separators));
            if (separators.Length == 0)
                throw new ArgumentException($"{nameof(separators)} array has to contain at least one separator string.");

            string[] inputLines = input.Split(separators, stringSplitOptions);

            int widthOfFirstLine = inputLines[0].Length;
            char[,] array = new char[inputLines.Length, widthOfFirstLine];

            for (int row = 0; row < inputLines.Length; row++)
            {
                if (inputLines[row].Length != widthOfFirstLine)
                {
                    throw new ArgumentException($"{nameof(input)}'s all rows has to be equal in width," +
                        $"but the width of row with index {row} differs from the width of first row.");
                }

                for (int col = 0; col < inputLines[row].Length; col++)
                {
                    array[row, col] = inputLines[row][col];
                }
            }

            return array;
        }
    }
}