﻿using System;

namespace GridPathFinder.Exceptions
{
    public sealed class MultipleSelfTilesException : ApplicationException
    {
        public int ErrorIndex { get; }

        public MultipleSelfTilesException(int errorIndex)
            : base($"Multiple {nameof(TileType.Self)} tiles found, the first duplicate is at index {errorIndex}.")
        {
            ErrorIndex = errorIndex;
        }
    }
}