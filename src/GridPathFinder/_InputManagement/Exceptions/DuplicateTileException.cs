﻿using System;

namespace GridPathFinder.Exceptions
{
    public sealed class DuplicateTileException : ApplicationException
    {
        public Tile DuplicateTile { get; }
        public int ErrorIndex { get; }

        public DuplicateTileException(Tile duplicateTile, int errorIndex)
            : base($"Duplicate tile found at index {errorIndex}. The tile was: {duplicateTile}.")
        {
            DuplicateTile = duplicateTile;
            ErrorIndex = errorIndex;
        }
    }
}