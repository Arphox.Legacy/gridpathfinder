﻿using System.Collections.Generic;

namespace GridPathFinder
{
    public sealed class TilePositionEqualityComparer : EqualityComparer<Tile>
    {
        public static readonly TilePositionEqualityComparer Instance = new TilePositionEqualityComparer();

        private TilePositionEqualityComparer() { }

        public override bool Equals(Tile x, Tile y)
        {
            if (ReferenceEquals(x, y))
                return true;

            if (x is null || y is null)
                return false;

            return x.Row == y.Row &&
                   x.Column == y.Column;
        }

        public override int GetHashCode(Tile obj)
        {
            if (obj is null)
                return 0;

            var hashCode = 564231447;
            hashCode = hashCode * -1521134295 + obj.Row.GetHashCode();
            hashCode = hashCode * -1521134295 + obj.Column.GetHashCode();

            return hashCode;
        }
    }
}