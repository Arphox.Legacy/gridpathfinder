﻿using GridPathFinder.Exceptions;
using System;
using System.Collections.Generic;

namespace GridPathFinder
{
    /// <summary>
    ///     Validates an <see cref="IEnumerable{T}"/> collection (of <see cref="Tile"/>s), checking for duplicates
    /// </summary>
    public static class TileCollectionValidator
    {
        /// <summary>
        ///     Validates an <see cref="IEnumerable{T}"/> of <see cref="Tile"/>s, checking the correctness of it
        /// </summary>
        /// <remarks>
        ///     - Throws <see cref="DuplicateTileException"/> if multiple tiles found on same position.
        ///     - Throws <see cref="MultipleSelfTilesException"/> if multiple <see cref="TileType.Self"/> tiles are found.
        ///     - Throws <see cref="TileNotFoundException"/> if no <see cref="TileType.Target"/> tile is found or
        ///         not exactly one <see cref="TileType.Self"/> tile found.
        /// </remarks>
        public static void Validate(IEnumerable<Tile> tiles)
        {
            if (tiles == null)
                throw new ArgumentNullException(nameof(tiles));

            var tileHashSet = new HashSet<Tile>(TilePositionEqualityComparer.Instance);

            int i = 0;
            bool hasSelfTile = false;
            bool hasTargetTile = false;
            foreach (var tile in tiles)
            {
                if (tileHashSet.Contains(tile))
                    throw new DuplicateTileException(tile, i);

                if (tile.Type == TileType.Self)
                {
                    if (hasSelfTile)
                        throw new MultipleSelfTilesException(i);
                    else
                        hasSelfTile = true;
                }
                else if (tile.Type == TileType.Target)
                {
                    hasTargetTile = true;
                }

                tileHashSet.Add(tile);
                i++;
            }

            if (!hasSelfTile)
                throw new TileNotFoundException($"No '{nameof(TileType.Self)}' tile found, expected exactly one.");
            if (!hasTargetTile)
                throw new TileNotFoundException($"No '{nameof(TileType.Target)}' tile found, expected at least one.");
        }
    }
}