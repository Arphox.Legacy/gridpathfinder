﻿using GridPathFinder.Utilities;
using System;
using System.Collections.Generic;

namespace GridPathFinder
{
    public sealed class TwoDimensionalArrayInputConverter<TInput>
    {
        public List<Tile> Convert(Func<TInput, TileType?> tileTypeConverter, TInput[,] inputArray)
        {
            if (tileTypeConverter == null)
                throw new ArgumentNullException(nameof(tileTypeConverter));
            if (inputArray == null)
                throw new ArgumentNullException(nameof(inputArray));

            if (inputArray.GetLength(0) < 1)
                throw new ArgumentException($"{nameof(inputArray)}'s 0. dimension's length has to be at least 1.");
            if (inputArray.GetLength(1) < 1)
                throw new ArgumentException($"{nameof(inputArray)}'s 1. dimension's length has to be at least 1.");

            int rowMax = inputArray.GetLength(0);
            int colMax = inputArray.GetLength(1);

            List<Tile> tiles = new List<Tile>(rowMax * colMax);

            for (int row = 0; row < rowMax; row++)
            {
                for (int col = 0; col < colMax; col++)
                {
                    Tile tile = GetTile(tileTypeConverter, inputArray, row, col);
                    if (tile != null)
                        tiles.Add(tile);
                }
            }

            return tiles;
        }

        private static Tile GetTile(Func<TInput, TileType?> converter, TInput[,] array, int row, int col)
        {
            TileType? tileType = converter(array[row, col]);
            if (tileType == null)
                return null;

            CheckTileTypeIsDefined(row, col, tileType.Value);

            Tile tile = new Tile(row, col, tileType.Value);
            return tile;
        }

        private static void CheckTileTypeIsDefined(int row, int col, TileType tileType)
        {
            if (!tileType.IsInRange())
            {
                throw new ArgumentException($"At index [{row},{col}]," +
                    $"the {nameof(TileType)} converter function returned a {nameof(TileType)} value" +
                    $"that is not defined in the enum. The invalid value is '{(int)tileType}'.");
            }
        }
    }
}