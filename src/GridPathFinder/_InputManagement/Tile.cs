﻿using GridPathFinder.Utilities;
using System;

namespace GridPathFinder
{
    public sealed class Tile : IEquatable<Tile>
    {
        public int Row { get; }
        public int Column { get; }
        public TileType Type { get; }

        public Tile(int row, int column, TileType type)
        {
            if (!type.IsInRange())
                throw new ArgumentOutOfRangeException($"{nameof(TileType)} with value of {(int)type} not found.");

            Row = row;
            Column = column;
            Type = type;
        }

        public override string ToString() => $"[{Row}, {Column}] ({Type})";

        #region [ Equality ]

        public bool Equals(Tile other)
        {
            if (other is null)
                return false;

            return Row == other.Row &&
                   Column == other.Column &&
                   Type == other.Type;
        }

        public override int GetHashCode()
        {
            var hashCode = 564231447;
            hashCode = hashCode * -1521134295 + Row.GetHashCode();
            hashCode = hashCode * -1521134295 + Column.GetHashCode();
            hashCode = hashCode * -1521134295 + Type.GetHashCode();
            return hashCode;
        }

        public override bool Equals(object obj)
        {
            if (obj is null)
                return false;

            if (obj is Tile tile)
                return Equals(tile);

            return false;
        }

        public static bool operator ==(Tile left, Tile right)
        {
            if (ReferenceEquals(left, right))
                return true;

            if (left is null || right is null)
                return false;

            return left.Equals(right);
        }

        public static bool operator !=(Tile left, Tile right) => !(left == right);

        #endregion
    }
}