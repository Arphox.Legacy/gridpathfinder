﻿using BenchmarkDotNet.Running;
using System;
using System.Diagnostics;

namespace GridPathFinder.PerformanceBenchmark
{
    internal static class Program
    {
        static void Main()
        {
#if DEBUG
            throw new Exception("Run this in RELEASE mode!");
#endif
            if (Debugger.IsAttached)
                throw new Exception("Do not attach a debugger!");

            var summary = BenchmarkRunner.Run<TileDistanceCalculatorPerformanceBenchmark>();
        }
    }
}