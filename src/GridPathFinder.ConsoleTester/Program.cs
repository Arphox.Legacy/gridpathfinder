﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace GridPathFinder.ConsoleTester
{
    class Program
    {
        static void Main()
        {
            //Stopwatch stopwatch = Stopwatch.StartNew();
            Run();
            //var elapsed = stopwatch.Elapsed;
            //Console.WriteLine(elapsed);
            //Console.ReadLine();
        }

        public static Dictionary<Tile, int> Run()
        {
            var tiles = GenerateTiles(20_000);
            var calculator = new TileDistanceCalculator(tiles);
            return calculator.CalculateTileDistances();
        }

        private static IEnumerable<Tile> GenerateTiles(int additionalTileCount)
        {
            if (additionalTileCount < 0)
                throw new ArgumentOutOfRangeException(nameof(additionalTileCount));

            Tile[] tiles = new Tile[additionalTileCount + 2];
            tiles[0] = new Tile(0, 0, TileType.Self);
            tiles[1] = new Tile(0, 1, TileType.Target);

            int columnCounter = 2;
            int limit = additionalTileCount + 2;
            for (int i = 2; i < limit; i++)
            {
                tiles[i] = new Tile(0, columnCounter, TileType.Empty);
                columnCounter++;
            }

            return tiles;
        }
    }
}