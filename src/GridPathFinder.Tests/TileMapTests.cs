﻿using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace GridPathFinder.Tests
{
    [TestFixture]
    public sealed class TileMapTests
    {
        [Test]
        public void Ctor_OnNullInput_ThrowsException()
        {
            // Arrange & Act
            Action action = () => new TileMap(null);

            // Assert
            Assert.Throws<ArgumentNullException>(() => action());
        }

        [Test]
        public void Ctor_OnCorrectInput_DoesNotThrowException()
        {
            // Arrange & Act
            Action action = () => new TileMap(new List<Tile>());

            // Assert
            Assert.DoesNotThrow(() => action());
        }

        [Test]
        public void Indexer_GetReturnsNull_WhenEmpty()
        {
            // Arrange
            var tileMap = new TileMap(new List<Tile>());

            // Act
            Tile result = tileMap[0, 0];

            // Assert
            Assert.That(result, Is.Null);
        }

        [Test]
        public void Indexer_GetReturnsNull_WhenNotFound_Row()
        {
            // Arrange
            var tiles = new List<Tile>()
            {
                new Tile(1, 1, TileType.Empty)
            };

            var tileMap = new TileMap(tiles);

            // Act
            Tile result = tileMap[0, 0];

            // Assert
            Assert.That(result, Is.Null);
        }

        [Test]
        public void Indexer_GetReturnsNull_WhenNotFound_Column()
        {
            // Arrange
            var tiles = new List<Tile>()
            {
                new Tile(1, 1, TileType.Empty)
            };

            var tileMap = new TileMap(tiles);

            // Act
            Tile result = tileMap[1, 0];

            // Assert
            Assert.That(result, Is.Null);
        }

        [Test]
        public void Indexer_GetReturnsCorrectValue()
        {
            // Arrange
            var tile1 = new Tile(1, 1, TileType.Empty);
            var tile2 = new Tile(2, 2, TileType.Target);

            var tiles = new List<Tile>() { tile1, tile2 };
            var tileMap = new TileMap(tiles);

            // Act
            Tile result = tileMap[tile1.Row, tile1.Column];

            // Assert
            Assert.That(result, Is.SameAs(tile1));
        }
    }
}