﻿using NUnit.Framework;
using System.Collections.Generic;

namespace GridPathFinder.Tests
{
    [TestFixture]
    public sealed class TileMap_NeighborTests
    {
        private static readonly Tile TileA = new Tile(0, 0, TileType.Empty);
        private static readonly Tile TileB = new Tile(0, 1, TileType.Self);
        private static readonly Tile TileC = new Tile(1, 0, TileType.Target);
        private static readonly Tile TileD = new Tile(1, 1, TileType.Wall);

        /// <summary>
        ///     C D
        ///     A B
        /// </summary>
        private static readonly Tile[] Tiles = new Tile[] { TileA, TileB, TileC, TileD, };

        [TestCaseSource(nameof(LeftNeighborTestCases))]
        public void LeftNeighborTest(Tile tileToCheck, Tile expectedNeighborTile)
        {
            var actualNeighbor = new TileMap(Tiles).GetLeftNeighborOf(tileToCheck);
            Assert.That(actualNeighbor, Is.SameAs(expectedNeighborTile));
        }

        [TestCaseSource(nameof(RightNeighborTestCases))]
        public void RightNeighborTest(Tile tileToCheck, Tile expectedNeighborTile)
        {
            var actualNeighbor = new TileMap(Tiles).GetRightNeighborOf(tileToCheck);
            Assert.That(actualNeighbor, Is.SameAs(expectedNeighborTile));
        }

        [TestCaseSource(nameof(UpNeighborTestCases))]
        public void UpNeighborTest(Tile tileToCheck, Tile expectedNeighborTile)
        {
            var actualNeighbor = new TileMap(Tiles).GetUpNeighborOf(tileToCheck);
            Assert.That(actualNeighbor, Is.SameAs(expectedNeighborTile));
        }

        [TestCaseSource(nameof(DownNeighborTestCases))]
        public void DownNeighborTest(Tile tileToCheck, Tile expectedNeighborTile)
        {
            var actualNeighbor = new TileMap(Tiles).GetDownNeighborOf(tileToCheck);
            Assert.That(actualNeighbor, Is.SameAs(expectedNeighborTile));
        }

        private static IEnumerable<TestCaseData> LeftNeighborTestCases()
        {
            int testCaseCounter = 1;

            yield return LeftOf(TileA, null);
            yield return LeftOf(TileB, TileA);
            yield return LeftOf(TileC, null);
            yield return LeftOf(TileD, TileC);

            // ----------------------------------------------------
            TestCaseData LeftOf(Tile sourceTile, Tile expectedTile)
            {
                return new TestCaseData(sourceTile, expectedTile)
                {
                    TestName = $"{nameof(TileMap.GetLeftNeighborOf)}_Test{testCaseCounter++}"
                };
            }
        }

        private static IEnumerable<TestCaseData> RightNeighborTestCases()
        {
            int testCaseCounter = 1;

            yield return RightOf(TileA, TileB);
            yield return RightOf(TileB, null);
            yield return RightOf(TileC, TileD);
            yield return RightOf(TileD, null);

            // -----------------------------------------------------
            TestCaseData RightOf(Tile sourceTile, Tile expectedTile)
            {
                return new TestCaseData(sourceTile, expectedTile)
                {
                    TestName = $"{nameof(TileMap.GetRightNeighborOf)}_Test{testCaseCounter++}"
                };
            }
        }

        private static IEnumerable<TestCaseData> UpNeighborTestCases()
        {
            int testCaseCounter = 1;

            yield return UpOf(TileA, TileC);
            yield return UpOf(TileB, TileD);
            yield return UpOf(TileC, null);
            yield return UpOf(TileD, null);

            // -----------------------------------------------------
            TestCaseData UpOf(Tile sourceTile, Tile expectedTile)
            {
                return new TestCaseData(sourceTile, expectedTile)
                {
                    TestName = $"{nameof(TileMap.GetUpNeighborOf)}_Test{testCaseCounter++}"
                };
            }
        }

        private static IEnumerable<TestCaseData> DownNeighborTestCases()
        {
            int testCaseCounter = 1;

            yield return DownOf(TileA, null);
            yield return DownOf(TileB, null);
            yield return DownOf(TileC, TileA);
            yield return DownOf(TileD, TileB);

            // -----------------------------------------------------
            TestCaseData DownOf(Tile sourceTile, Tile expectedTile)
            {
                return new TestCaseData(sourceTile, expectedTile)
                {
                    TestName = $"{nameof(TileMap.GetDownNeighborOf)}_Test{testCaseCounter++}"
                };
            }
        }
    }
}