﻿using System;

namespace GridPathFinder.Tests.TestHelpers
{
    public static class TestCommon
    {
        /// <summary>
        ///     'S' = Self, 'T' = Target, '.' = Empty, '#' = Wall, ' ' = Nothing (void)
        /// </summary>
        public static readonly Func<char, TileType?> STdotHashFunc = new Func<char, TileType?>(ch =>
        {
            switch (ch)
            {
                case '#': return TileType.Wall;
                case '.': return TileType.Empty;
                case 'S': return TileType.Self;
                case 'T': return TileType.Target;
                case ' ': return null;
                default: throw new InvalidOperationException();
            }
        });
    }
}