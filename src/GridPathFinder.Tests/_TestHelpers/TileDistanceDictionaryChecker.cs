﻿using NUnit.Framework;
using System.Collections.Generic;

namespace GridPathFinder.Tests._TestHelpers
{
    internal sealed class TileDistanceDictionaryChecker
    {
        private readonly List<Tile> originalTiles;
        private readonly Dictionary<Tile, int> tileDistances;
        private int counter = 0;

        internal TileDistanceDictionaryChecker(List<Tile> originalTiles, Dictionary<Tile, int> tileDistances)
        {
            this.originalTiles = originalTiles;
            this.tileDistances = tileDistances;
        }

        internal TileDistanceDictionaryChecker CheckNextTilesInOrder(params int[] expectedValues)
        {
            foreach (int value in expectedValues)
                CheckNextTile(value);

            return this;
        }

        private void CheckNextTile(int expectedValue)
        {
            Tile originalTile = originalTiles[counter++];
            int actualValue = tileDistances[originalTile];
            Assert.That(actualValue, Is.EqualTo(expectedValue));
        }

        internal void NoMoreTiles()
        {
            Assert.That(originalTiles, Has.Count.EqualTo(counter));
            Assert.That(tileDistances, Has.Count.EqualTo(counter));
        }
    }
}