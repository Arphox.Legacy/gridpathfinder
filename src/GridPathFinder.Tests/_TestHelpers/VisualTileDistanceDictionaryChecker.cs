﻿using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace GridPathFinder.Tests._TestHelpers
{
    internal sealed class VisualTileDistanceDictionaryChecker
    {
        private readonly List<Tile> originalTiles;
        private readonly Dictionary<Tile, int> tileDistances;
        private readonly string input;
        private int counter = 0;

        internal VisualTileDistanceDictionaryChecker(
            List<Tile> originalTiles,
            Dictionary<Tile, int> tileDistances,
            string input)
        {
            this.originalTiles = originalTiles;
            this.tileDistances = tileDistances;
            this.input = input;
        }

        internal void Validate()
        {
            string[] inputLines = input.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            for (int row = 0; row < inputLines.Length; row++)
            {
                for (int col = 0; col < inputLines[row].Length; col++)
                {
                    char validationChar = inputLines[row][col];

                    switch (validationChar)
                    {
                        case ' ': break;
                        case '#': CheckNextTile(-1); break;
                        default: CheckNextTile(validationChar - '0'); break;
                    }
                }
            }

            NoMoreTiles();
        }

        private void CheckNextTile(int expectedValue)
        {
            Tile originalTile = originalTiles[counter++];
            int actualValue = tileDistances[originalTile];
            Assert.That(actualValue, Is.EqualTo(expectedValue),
                $"Value mismatch at row {originalTile.Row} col {originalTile.Column}.");
        }

        internal void NoMoreTiles()
        {
            Assert.That(originalTiles, Has.Count.EqualTo(counter));
            Assert.That(tileDistances, Has.Count.EqualTo(counter));
        }
    }
}
