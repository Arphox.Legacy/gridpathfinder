﻿using GridPathFinder.Exceptions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GridPathFinder.Tests.TileCollectionValidatorInput
{
    [TestFixture]
    public sealed class TileCollectionValidatorTests
    {
        private static readonly IReadOnlyList<Tile> sampleTiles = new List<Tile>()
        {
            new Tile(0, 0, TileType.Wall),
            new Tile(1, 0, TileType.Self),
            new Tile(0, 1, TileType.Target),
            new Tile(1, 1, TileType.Wall),
        };

        [Test]
        public void Validate_OnNullInput_ThrowsArgumentNullException()
        {
            // Act (delayed)
            Action action = () => Act(null);

            // Assert
            Assert.Throws<ArgumentNullException>(() => action());
        }

        [Test]
        public void Validate_DoesNotThrowException_IfValid()
        {
            // Arrange
            var tiles = sampleTiles.ToList();
            tiles.Add(new Tile(1, 2, TileType.Target));

            // Act (delayed)
            Action action = () => Act(tiles);

            // Assert
            Assert.DoesNotThrow(() => action());
        }

        [Test]
        public void Validate_ThrowsDuplicateTileException_WhenNeeded()
        {
            // Arrange
            List<Tile> tiles = sampleTiles.ToList();
            tiles.Add(new Tile(1, 0, TileType.Wall));

            // Act (delayed)
            Action action = () => Act(tiles);

            // Assert
            Assert.Throws<DuplicateTileException>(() => action());
        }

        [Test]
        public void Validate_ThrowsDuplicateTileException_WhenNeeded_WithCorrectData()
        {
            // Arrange
            const int expectedErrorIndex = 3;
            List<Tile> tiles = sampleTiles.ToList();
            var duplicateTile = new Tile(1, 0, TileType.Wall);
            tiles.Insert(expectedErrorIndex, duplicateTile);

            // Act (delayed)
            Action action = () => Act(tiles);

            // Assert
            try
            {
                action();
                Assert.Fail("Action did not throw exception");
            }
            catch (DuplicateTileException e)
            {
                Assert.That(e.DuplicateTile, Is.EqualTo(duplicateTile));
                Assert.That(e.ErrorIndex, Is.EqualTo(expectedErrorIndex));
            }
        }

        [Test]
        public void Validate_ThrowsDuplicateTileException_WithCorrectIndex_IfMultipleDuplicates()
        {
            // Arrange
            const int expectedErrorIndex = 3;
            List<Tile> tiles = sampleTiles.ToList();
            var duplicateTile = new Tile(1, 0, TileType.Wall);
            tiles.Insert(expectedErrorIndex, duplicateTile);
            tiles.Insert(expectedErrorIndex + 2, duplicateTile);

            // Act (delayed)
            Action action = () => Act(tiles);

            // Assert
            try
            {
                action();
                Assert.Fail("Action did not throw exception");
            }
            catch (DuplicateTileException e)
            {
                Assert.That(e.ErrorIndex, Is.EqualTo(expectedErrorIndex));
            }
        }

        [Test]
        public void Validate_ThrowsMultipleSelfTilesException_WhenNeeded()
        {
            // Arrange
            List<Tile> tiles = sampleTiles.ToList();
            tiles.Add(new Tile(10, 0, TileType.Self));

            // Act (delayed)
            Action action = () => Act(tiles);

            // Assert
            Assert.Throws<MultipleSelfTilesException>(() => action());
        }

        [Test]
        public void Validate_ThrowsMultipleSelfTilesException_WhenNeeded_WithCorrectIndex()
        {
            // Arrange
            List<Tile> tiles = sampleTiles.ToList();
            const int expectedErrorIndex = 3;
            tiles.Insert(expectedErrorIndex, new Tile(10, 0, TileType.Self));

            // Act (delayed)
            Action action = () => Act(tiles);

            // Assert
            try
            {
                action();
                Assert.Fail("Action did not throw exception");
            }
            catch (MultipleSelfTilesException e)
            {
                Assert.That(e.ErrorIndex, Is.EqualTo(expectedErrorIndex));
            }
        }

        [Test]
        public void Validate_ThrowsMultipleSelfTilesException_WhenNeeded_WithCorrectIndex_IfMultipleSelf()
        {
            // Arrange
            List<Tile> tiles = sampleTiles.ToList();
            const int expectedErrorIndex = 3;
            tiles.Insert(expectedErrorIndex, new Tile(10, 0, TileType.Self));
            tiles.Insert(expectedErrorIndex + 2, new Tile(10, 0, TileType.Self));

            // Act (delayed)
            Action action = () => Act(tiles);

            // Assert
            try
            {
                action();
                Assert.Fail("Action did not throw exception");
            }
            catch (MultipleSelfTilesException e)
            {
                Assert.That(e.ErrorIndex, Is.EqualTo(expectedErrorIndex));
            }
        }

        [Test]
        public void Validate_ThrowsTileNotFoundException_WhenNoSelfTilesFound()
        {
            // Arrange
            List<Tile> tiles = sampleTiles.ToList();
            tiles.RemoveAll(t => t.Type == TileType.Self);

            // Act (delayed)
            Action action = () => Act(tiles);

            // Assert
            Assert.Throws<TileNotFoundException>(() => action());
        }

        [Test]
        public void Validate_ThrowsTileNotFoundException_WhenNoTargetTilesFound()
        {
            // Arrange
            List<Tile> tiles = sampleTiles.ToList();
            tiles.RemoveAll(t => t.Type == TileType.Target);

            // Act (delayed)
            Action action = () => Act(tiles);

            // Assert
            Assert.Throws<TileNotFoundException>(() => action());
        }

        private static void Act(IEnumerable<Tile> tiles)
            => TileCollectionValidator.Validate(tiles);
    }
}