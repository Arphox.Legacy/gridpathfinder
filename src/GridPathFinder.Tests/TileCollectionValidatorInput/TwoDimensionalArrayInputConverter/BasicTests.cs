﻿using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace GridPathFinder.Tests.TileCollectionValidatorInput.TwoDimensionalArrayInputConverter
{
    /// <summary>
    ///     Contains basic tests for <see cref="TwoDimensionalArrayInputConverter{T}"/>
    /// </summary>
    [TestFixture]
    public sealed class BasicTests
    {
        private static Func<int, TileType?> DummyIntFunc => x => TileType.Empty;

        [Test]
        public void Convert_OnNullParam_ThrowsArgumentNullException()
        {
            Assert.Multiple(() =>
            {
                Assert.Throws<ArgumentNullException>(() => new TwoDimensionalArrayInputConverter<int>().Convert(null, null));
                Assert.Throws<ArgumentNullException>(() => new TwoDimensionalArrayInputConverter<int>().Convert(null, new int[1, 2]));
                Assert.Throws<ArgumentNullException>(() => new TwoDimensionalArrayInputConverter<int>().Convert(DummyIntFunc, null));
            });
        }

        [TestCaseSource(nameof(InvalidArrayTestCases))]
        public void Convert_OnInvalidArray_ThrowsArgumentException(int[,] array)
        {
            Assert.Throws<ArgumentException>(() => new TwoDimensionalArrayInputConverter<int>().Convert(DummyIntFunc, array));
        }

        private static IEnumerable<TestCaseData> InvalidArrayTestCases()
        {
            string testNameBase = nameof(Convert_OnInvalidArray_ThrowsArgumentException);
            int counter = 1;

            yield return new TestCaseData(new int[0, 0]) { TestName = testNameBase + counter++ };
            yield return new TestCaseData(new int[0, 1]) { TestName = testNameBase + counter++ };
            yield return new TestCaseData(new int[1, 0]) { TestName = testNameBase + counter++ };
        }

        [Test]
        public void Convert_IfFuncReturnsInvalidTileType_ThrowsException()
        {
            // Arrange
            Func<int, TileType?> func = new Func<int, TileType?>(x =>
            {
                switch (x)
                {
                    case 1: return TileType.Wall;
                    case 9: return (TileType)6000;
                    default: throw new InvalidOperationException();
                }
            });

            int[,] array = new int[,]
            {
                { 1, 1 },
                { 9, 1 }
            };

            var converter = new TwoDimensionalArrayInputConverter<int>();

            // Act (delayed)
            Action action = () => converter.Convert(func, array);

            // Assert
            Assert.Throws<ArgumentException>(() => action());
        }
    }
}