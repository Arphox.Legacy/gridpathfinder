﻿using GridPathFinder.Utilities;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using static GridPathFinder.Tests.TestHelpers.TestCommon;

namespace GridPathFinder.Tests.TileCollectionValidatorInput.TwoDimensionalArrayInputConverter
{
    /// <summary>
    ///     Contains logic validation tests for <see cref="TwoDimensionalArrayInputConverter{T}"/>
    /// </summary>
    [TestFixture]
    public sealed class LogicTests
    {
        [TestCase('#', TileType.Wall)]
        [TestCase('.', TileType.Empty)]
        [TestCase('S', TileType.Self)]
        [TestCase('T', TileType.Target)]
        public void Basic_1row1col_CallsFuncCorrectly(char input, TileType expectedTileType)
        {
            // Arrange
            char[,] array = input.ToString().To2DCharArray(new string[] { "\n" });
            var converter = new TwoDimensionalArrayInputConverter<char>();

            // Act
            List<Tile> tiles = converter.Convert(STdotHashFunc, array);

            // Assert
            int i = 0;
            Assert.That(tiles, Has.Count.EqualTo(array.Length));
            CheckTile(tiles[i++], 0, 0, expectedTileType);
        }

        [Test]
        public void Basic_2row_1col()
        {
            // Arrange
            char[,] array = new char[,]
            {
                { '#' },
                { 'S' }
            };

            var converter = new TwoDimensionalArrayInputConverter<char>();

            // Act
            List<Tile> tiles = converter.Convert(STdotHashFunc, array);

            // Assert
            int i = 0;
            Assert.That(tiles, Has.Count.EqualTo(array.Length));
            CheckTile(tiles[i++], 0, 0, TileType.Wall);
            CheckTile(tiles[i++], 1, 0, TileType.Self);
        }

        [Test]
        public void Basic_1row_2col()
        {
            // Arrange
            char[,] array = new char[,]
            {
                { '#', '.' }
            };
            var converter = new TwoDimensionalArrayInputConverter<char>();

            // Act
            List<Tile> tiles = converter.Convert(STdotHashFunc, array);

            // Assert
            int i = 0;
            Assert.That(tiles, Has.Count.EqualTo(array.Length));
            CheckTile(tiles[i++], 0, 0, TileType.Wall);
            CheckTile(tiles[i++], 0, 1, TileType.Empty);
        }

        [Test]
        public void Basic_2row_2col()
        {
            // Arrange
            char[,] array = new char[,]
            {
                { '#', '.' },
                { 'S', 'T' }
            };
            var converter = new TwoDimensionalArrayInputConverter<char>();

            // Act
            List<Tile> tiles = converter.Convert(STdotHashFunc, array);

            // Assert
            int i = 0;
            Assert.That(tiles, Has.Count.EqualTo(array.Length));
            CheckTile(tiles[i++], 0, 0, TileType.Wall);
            CheckTile(tiles[i++], 0, 1, TileType.Empty);
            CheckTile(tiles[i++], 1, 0, TileType.Self);
            CheckTile(tiles[i++], 1, 1, TileType.Target);
        }

        [Test]
        public void Basic_4row_3col()
        {
            // Arrange
            char[,] array = new char[,]
            {
                { '#', '#', '#' },
                { '#', 'S', '#' },
                { '#', 'T', '.' },
                { '.', '#', '#' },
            };
            var converter = new TwoDimensionalArrayInputConverter<char>();

            // Act
            List<Tile> tiles = converter.Convert(STdotHashFunc, array);

            // Assert
            int i = 0;
            Assert.That(tiles, Has.Count.EqualTo(array.Length));
            CheckTile(tiles[i++], 0, 0, TileType.Wall);
            CheckTile(tiles[i++], 0, 1, TileType.Wall);
            CheckTile(tiles[i++], 0, 2, TileType.Wall);

            CheckTile(tiles[i++], 1, 0, TileType.Wall);
            CheckTile(tiles[i++], 1, 1, TileType.Self);
            CheckTile(tiles[i++], 1, 2, TileType.Wall);

            CheckTile(tiles[i++], 2, 0, TileType.Wall);
            CheckTile(tiles[i++], 2, 1, TileType.Target);
            CheckTile(tiles[i++], 2, 2, TileType.Empty);

            CheckTile(tiles[i++], 3, 0, TileType.Empty);
            CheckTile(tiles[i++], 3, 1, TileType.Wall);
            CheckTile(tiles[i++], 3, 2, TileType.Wall);
        }

        [Test]
        public void SimpleCross()
        {
            // Arrange
            char[,] array = new char[,]
            {
                { ' ', '#', ' ' },
                { '#', 'S', '#' },
                { ' ', '#', ' ' },
            };
            var converter = new TwoDimensionalArrayInputConverter<char>();

            // Act
            List<Tile> tiles = converter.Convert(STdotHashFunc, array);

            // Assert
            int i = 0;
            Assert.That(tiles, Has.Count.EqualTo(array.Length - GetNumberOfSpacesIn(array)));
            CheckTile(tiles[i++], 0, 1, TileType.Wall);

            CheckTile(tiles[i++], 1, 0, TileType.Wall);
            CheckTile(tiles[i++], 1, 1, TileType.Self);
            CheckTile(tiles[i++], 1, 2, TileType.Wall);

            CheckTile(tiles[i++], 2, 1, TileType.Wall);
        }

        [Test]
        public void RandomSpaces()
        {
            // Arrange
            char[,] array = new char[,]
            {
                { 'T', ' ', ' ' },
                { '.', '.', ' ' },
                { ' ', '.', 'S' },
            };
            var converter = new TwoDimensionalArrayInputConverter<char>();

            // Act
            List<Tile> tiles = converter.Convert(STdotHashFunc, array);

            // Assert
            int i = 0;
            Assert.That(tiles, Has.Count.EqualTo(array.Length - GetNumberOfSpacesIn(array)));
            CheckTile(tiles[i++], 0, 0, TileType.Target);

            CheckTile(tiles[i++], 1, 0, TileType.Empty);
            CheckTile(tiles[i++], 1, 1, TileType.Empty);

            CheckTile(tiles[i++], 2, 1, TileType.Empty);
            CheckTile(tiles[i++], 2, 2, TileType.Self);
        }

        private static void CheckTile(Tile tile, int expectedRow, int expectedColumn, TileType expectedType)
        {
            Assert.That(tile.Row, Is.EqualTo(expectedRow));
            Assert.That(tile.Column, Is.EqualTo(expectedColumn));
            Assert.That(tile.Type, Is.EqualTo(expectedType));
        }

        private static int GetNumberOfSpacesIn(char[,] array) => array.Cast<char>().Count(x => x == ' ');
    }
}