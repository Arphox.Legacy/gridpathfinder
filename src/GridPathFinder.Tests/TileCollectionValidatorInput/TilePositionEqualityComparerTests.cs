﻿using NUnit.Framework;
using System.Collections.Generic;
using static GridPathFinder.Tests.TileCollectionValidatorInput.ExampleTileObjects;

namespace GridPathFinder.Tests.TileCollectionValidatorInput
{
    [TestFixture]
    public sealed class TilePositionEqualityComparerTests
    {
        private static readonly TilePositionEqualityComparer comparer = TilePositionEqualityComparer.Instance;

        [TestCaseSource(nameof(TestCaseDatas_NullCheck))]
        [TestCaseSource(nameof(TestCaseDatas_SameReferenceOrData))]
        [TestCaseSource(nameof(TestCaseDatas_DifferentPositionSameType))]
        public void TestHashCodeAndEquality(Tile a, Tile b, bool shouldBeEqual)
        {
            bool result_typedEquals = comparer.Equals(a, b);
            bool result_objectEquals = comparer.Equals(b, a);
            bool result_hashCodeEqual = comparer.GetHashCode(a) == comparer.GetHashCode(b);

            Assert.Multiple(() =>
            {
                Assert.That(result_typedEquals, Is.EqualTo(shouldBeEqual), $"Equals(Tile) method failed when comparing: '{a}' and '{b}'");
                Assert.That(result_objectEquals, Is.EqualTo(shouldBeEqual), $"Equals(object) method failed when comparing: '{a}' and '{b}'");
                Assert.That(result_hashCodeEqual, Is.EqualTo(shouldBeEqual), $"Hashcodes of tiles '{a}' and '{b}' do not match.");
            });
        }

        private static IEnumerable<TestCaseData> TestCaseDatas_NullCheck()
        {
            yield return new TestCaseData(null, null, true) { TestName = "TestNullBothSide" };
            yield return new TestCaseData(null, Empty_1_1(), false) { TestName = "TestNullLeftSide" };
            yield return new TestCaseData(Empty_1_1(), null, false) { TestName = "TestNullRightSide" };
        }

        private static IEnumerable<TestCaseData> TestCaseDatas_SameReferenceOrData()
        {
            var reference = Empty_1_1();
            yield return new TestCaseData(reference, reference, true) { TestName = "TestSameReference" };

            int counter = 0;
            yield return SameDataTestCase(Empty_1_1(), Empty_1_1());
            yield return SameDataTestCase(Target_33_3(), Target_33_3());
            yield return SameDataTestCase(Wall_9_12(), Wall_9_12());

            // ---------------------------------------------------
            TestCaseData SameDataTestCase(Tile left, Tile right)
            {
                return new TestCaseData(left, right, true) { TestName = "TestSamePositionSameType_" + counter++ };
            }
        }

        private static IEnumerable<TestCaseData> TestCaseDatas_DifferentPositionSameType()
        {
            int counter = 0;
            yield return CreateTestCaseData(Empty_1_11(), Empty_1_1());
            yield return CreateTestCaseData(Wall_4_4(), Wall_9_12());
            yield return CreateTestCaseData(Target_33_3(), Target_9_12());
            yield return CreateTestCaseData(Self_2_2(), Self_9_12());

            // ---------------------------------------------------
            TestCaseData CreateTestCaseData(Tile left, Tile right)
            {
                return new TestCaseData(left, right, false) { TestName = "TestDifferentPositionSameType_" + counter++ };
            }
        }
    }
}