﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using static GridPathFinder.Tests.TileCollectionValidatorInput.ExampleTileObjects;

namespace GridPathFinder.Tests.TileCollectionValidatorInput
{
    [TestFixture]
    public sealed class TileEqualityTests
    {
        [Test]
        public void TestNullEquality()
        {
            Assert.Multiple(() =>
            {
                Assert.That(new Tile(0, 0, TileType.Empty).Equals(null), Is.EqualTo(false));
                Assert.That(new Tile(0, 0, TileType.Empty) == null, Is.EqualTo(false));
                Assert.That(null == new Tile(0, 0, TileType.Empty), Is.EqualTo(false));
                Assert.That(new Tile(0, 0, TileType.Empty) != null, Is.Not.EqualTo(false));
                Assert.That(null != new Tile(0, 0, TileType.Empty), Is.Not.EqualTo(false));

                Assert.That(new Tile(0, 0, TileType.Empty).Equals((object)null), Is.EqualTo(false));
                Assert.That(new Tile(0, 0, TileType.Empty) == (object)null, Is.EqualTo(false));
                Assert.That(new Tile(0, 0, TileType.Empty) != (object)null, Is.Not.EqualTo(false));
            });
        }

        [Test]
        public void TestEquality_WithOtherType()
        {
            Assert.Multiple(() =>
            {
                Assert.That(new Tile(0, 0, TileType.Empty).Equals(2), Is.EqualTo(false));
                Assert.That(new Tile(0, 0, TileType.Empty).Equals("asd"), Is.EqualTo(false));
                Assert.That(new Tile(0, 0, TileType.Empty).Equals(new object()), Is.EqualTo(false));
                Assert.That(new Tile(0, 0, TileType.Empty).Equals(new System.Random()), Is.EqualTo(false));
            });
        }

        [TestCaseSource(nameof(GetReferenceEqualityTestCases))]
        [TestCaseSource(nameof(GetSameDataTestCases))]
        [TestCaseSource(nameof(GetDifferentDataTestCases))]
        public void TestHashCodeAndEquality(Tile a, Tile b, bool shouldBeEqual)
        {
            bool result_typedEquals = a.Equals(b);
            bool result_objectEquals = a.Equals((object)b);
            bool result_OpEqual = a == b && b == a;
            bool result_OpNotEqual = a != b && b != a;
            bool result_hashCodeEqual = a.GetHashCode() == b.GetHashCode();

            Assert.Multiple(() =>
            {
                Assert.That(result_typedEquals, Is.EqualTo(shouldBeEqual), $"Equals(Tile) method failed when comparing: '{a}' and '{b}'");
                Assert.That(result_objectEquals, Is.EqualTo(shouldBeEqual), $"Equals(object) method failed when comparing: '{a}' and '{b}'");
                Assert.That(result_OpEqual, Is.EqualTo(shouldBeEqual), $"operator == failed when comparing: '{a}' and '{b}'");
                Assert.That(result_OpNotEqual, Is.Not.EqualTo(shouldBeEqual), $"operator != failed when comparing: '{a}' and '{b}'");
                Assert.That(result_hashCodeEqual, Is.EqualTo(shouldBeEqual), $"Hashcodes of tiles '{a}' and '{b}' do not match.");
            });
        }

        #region [ TestCaseData generators ]

        private static IEnumerable<TestCaseData> GetReferenceEqualityTestCases()
        {
            return CreateSameReferenceTestCasesFor(
                Empty_1_1(),
                Empty_11_1(),
                Empty_1_11(),
                Self_2_2(),
                Self_22_2(),
                Self_2_22(),
                Target_3_3(),
                Target_3_33(),
                Target_33_3(),
                Wall_4_4(),
                Wall_4_44(),
                Wall_44_4()
            );

            // ----------------------------------------------------------------------
            IEnumerable<TestCaseData> CreateSameReferenceTestCasesFor(params Tile[] tiles)
            {
                int counter = 1;
                foreach (var item in tiles)
                    yield return Create(item, item, true, $"TestSameReference_{counter++:D2}");
            }
        }

        private static IEnumerable<TestCaseData> GetSameDataTestCases()
        {
            return CreateSameDataTestCasesFor(
                Empty_1_1,
                Empty_11_1,
                Empty_1_11,
                Self_2_2,
                Self_22_2,
                Self_2_22,
                Target_3_3,
                Target_3_33,
                Target_33_3,
                Wall_4_4,
                Wall_4_44,
                Wall_44_4);

            // ----------------------------------------------------------------------
            IEnumerable<TestCaseData> CreateSameDataTestCasesFor(params Func<Tile>[] tiles)
            {
                int counter = 1;

                foreach (var item in tiles)
                    yield return Create(item(), item(), true, $"TestSameData_{counter++:D2}");
            }
        }

        private static IEnumerable<TestCaseData> GetDifferentDataTestCases()
        {
            int counter = 1;

            yield return CreateDifferentDataTestCase(Empty_1_1(), Empty_11_1());
            yield return CreateDifferentDataTestCase(Empty_11_1(), Empty_1_11());
            yield return CreateDifferentDataTestCase(Empty_1_11(), Self_2_2());

            yield return CreateDifferentDataTestCase(Self_2_2(), Self_22_2());
            yield return CreateDifferentDataTestCase(Self_22_2(), Self_2_22());
            yield return CreateDifferentDataTestCase(Self_2_22(), Target_3_3());

            yield return CreateDifferentDataTestCase(Target_3_3(), Target_3_33());
            yield return CreateDifferentDataTestCase(Target_3_33(), Target_33_3());
            yield return CreateDifferentDataTestCase(Target_33_3(), Wall_4_4());

            yield return CreateDifferentDataTestCase(Wall_4_4(), Wall_4_44());
            yield return CreateDifferentDataTestCase(Wall_4_44(), Wall_44_4());
            yield return CreateDifferentDataTestCase(Wall_44_4(), Empty_1_1());

            yield return CreateDifferentDataTestCase(Empty_1_1(), Self_2_22());
            yield return CreateDifferentDataTestCase(Self_2_2(), Target_33_3());
            yield return CreateDifferentDataTestCase(Target_3_3(), Wall_44_4());
            yield return CreateDifferentDataTestCase(Wall_4_4(), Empty_1_11());

            yield return CreateDifferentDataTestCase(Empty_1_1(), Target_33_3());
            yield return CreateDifferentDataTestCase(Self_22_2(), Wall_44_4());
            yield return CreateDifferentDataTestCase(Target_3_33(), Wall_44_4());
            yield return CreateDifferentDataTestCase(Self_22_2(), Empty_11_1());

            yield return CreateDifferentDataTestCase(Empty_9_12(), Target_9_12());
            yield return CreateDifferentDataTestCase(Target_9_12(), Self_9_12());
            yield return CreateDifferentDataTestCase(Self_9_12(), Wall_9_12());
            yield return CreateDifferentDataTestCase(Wall_9_12(), Empty_9_12());
            yield return CreateDifferentDataTestCase(Empty_9_12(), Self_9_12());
            yield return CreateDifferentDataTestCase(Target_9_12(), Wall_9_12());

            // ----------------------------------------------------------------------
            TestCaseData CreateDifferentDataTestCase(Tile left, Tile right)
            {
                return Create(left, right, false, $"TestDifferentData_{counter++:D2}");
            }
        }

        #endregion

        private static TestCaseData Create(object a, object b, bool shouldBeEqual, string testName)
        {
            return new TestCaseData(a, b, shouldBeEqual) { TestName = testName };
        }
    }
}