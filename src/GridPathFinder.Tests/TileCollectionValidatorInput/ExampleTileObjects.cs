﻿namespace GridPathFinder.Tests.TileCollectionValidatorInput
{
    internal static class ExampleTileObjects
    {
        internal static Tile Empty_1_1() => new Tile(1, 1, TileType.Empty);
        internal static Tile Empty_11_1() => new Tile(11, 1, TileType.Empty);
        internal static Tile Empty_1_11() => new Tile(1, 11, TileType.Empty);
        
        internal static Tile Self_2_2() => new Tile(2, 2, TileType.Self);
        internal static Tile Self_22_2() => new Tile(22, 2, TileType.Self);
        internal static Tile Self_2_22() => new Tile(2, 22, TileType.Self);
        
        internal static Tile Target_3_3() => new Tile(3, 3, TileType.Target);
        internal static Tile Target_3_33() => new Tile(3, 33, TileType.Target);
        internal static Tile Target_33_3() => new Tile(33, 3, TileType.Target);
        
        internal static Tile Wall_4_4() => new Tile(4, 4, TileType.Wall);
        internal static Tile Wall_4_44() => new Tile(4, 44, TileType.Wall);
        internal static Tile Wall_44_4() => new Tile(44, 4, TileType.Wall);
        
        internal static Tile Empty_9_12() => new Tile(9, 12, TileType.Empty);
        internal static Tile Wall_9_12() => new Tile(9, 12, TileType.Wall);
        internal static Tile Self_9_12() => new Tile(9, 12, TileType.Self);
        internal static Tile Target_9_12() => new Tile(9, 12, TileType.Target);
    }
}