﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace GridPathFinder.Tests
{
    [TestFixture]
    [NonParallelizable]
    [Explicit]
    public sealed class TileDistanceCalculator_PerformanceTests
    {
        [Test]
        public void CalculateTileDistances_PerformanceTest1()
        {
            // Arrange
            var tiles = GenerateTiles(1_000);
            var calculator = new TileDistanceCalculator(tiles);

            // Act
            Stopwatch stopwatch = Stopwatch.StartNew();
            calculator.CalculateTileDistances();
            TimeSpan elapsedTime = stopwatch.Elapsed;

            // Assert
            Assert.That(elapsedTime, Is.LessThan(TimeSpan.FromMilliseconds(500)));
        }

        private static IEnumerable<Tile> GenerateTiles(int additionalTileCount)
        {
            if (additionalTileCount < 0)
                throw new ArgumentOutOfRangeException(nameof(additionalTileCount));

            Tile[] tiles = new Tile[additionalTileCount + 2];
            tiles[0] = new Tile(0, 0, TileType.Self);
            tiles[1] = new Tile(0, 1, TileType.Target);

            int columnCounter = 2;
            int limit = additionalTileCount + 2;
            for (int i = 2; i < limit; i++)
            {
                tiles[i] = new Tile(0, columnCounter, TileType.Empty);
                columnCounter++;
            }

            return tiles;
        }
    }
}