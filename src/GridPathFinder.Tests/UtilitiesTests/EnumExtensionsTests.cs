﻿using GridPathFinder.Utilities;
using NUnit.Framework;

namespace GridPathFinder.Tests.UtilitiesTests
{
    [TestFixture]
    public sealed class EnumExtensionsTests
    {
        public enum TestEnum
        {
            A = -1,
            B = 4,
            C = 76
        }

        [TestCase(TestEnum.A, true)]
        [TestCase(TestEnum.B, true)]
        [TestCase(TestEnum.C, true)]
        [TestCase((TestEnum)int.MinValue, false)]
        [TestCase((TestEnum)(-5602), false)]
        [TestCase((TestEnum)(-4), false)]
        [TestCase((TestEnum)0, false)]
        [TestCase((TestEnum)1, false)]
        [TestCase((TestEnum)2, false)]
        [TestCase((TestEnum)3, false)]
        [TestCase((TestEnum)77, false)]
        [TestCase((TestEnum)int.MaxValue, false)]
        public void IsInRangeTests_TestEnum(TestEnum value, bool isInRange)
        {
            Assert.That(value.IsInRange(), Is.EqualTo(isInRange));
        }
    }
}