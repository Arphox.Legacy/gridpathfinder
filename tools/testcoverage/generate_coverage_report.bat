REM Created by Arphox with a lot of suffering :D
REM @ECHO OFF

REM SETLOCAL: "Call the SETLOCAL command to make variables local to the scope of your script."
SETLOCAL

REM set variables
SET my_dotnetcoreFolder="c:\Program Files\dotnet\dotnet.exe"
SET my_startingDir=%CD%
SET my_reportDir="%TEMP%\___opencover_generated_report\"
SET my_openCoverPath="%my_startingDir%\opencover.4.7.922\OpenCover.Console.exe"
SET my_reportGenPath="%my_startingDir%\ReportGenerator_4.1.7\ReportGenerator.exe"
SET my_coveragexmlpath="%TEMP%\my_coverage.xml"

REM Delete report dir, if exists
rmdir %my_reportDir% /s /q

REM CD to test project folder
CD ..\..\src\GridPathFinder.Tests\

REM Start coverage measurement
%my_openCoverPath% -register:user -target:%my_dotnetcoreFolder% -targetargs:"test" -oldStyle -output:%my_coveragexmlpath% -filter:"+[GridPathFinder*]* -[GridPathFinder*Test*]*" -excludebyattribute:*.ExcludeFromCodeCoverage*

REM Start report generation
%my_reportGenPath% -reports:%my_coveragexmlpath% -targetdir:%my_reportDir%

REM Delete coverage.xml file, as it is not needed anymore
del %my_coveragexmlpath%

REM Show report in default browser
explorer "%my_reportDir%index.htm"